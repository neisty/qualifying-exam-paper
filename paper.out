\BOOKMARK [1][-]{section.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.2}{Bridging The Chasm: A Survey Of Software Engineering Practices In Scientific Programming Storer2017}{}% 2
\BOOKMARK [2][-]{subsection.2.1}{Case study}{section.2}% 3
\BOOKMARK [2][-]{subsection.2.2}{Practices}{section.2}% 4
\BOOKMARK [2][-]{subsection.2.3}{Methods}{section.2}% 5
\BOOKMARK [2][-]{subsection.2.4}{New Approaches}{section.2}% 6
\BOOKMARK [2][-]{subsection.2.5}{Data Quality}{section.2}% 7
\BOOKMARK [2][-]{subsection.2.6}{Developers}{section.2}% 8
\BOOKMARK [2][-]{subsection.2.7}{Management}{section.2}% 9
\BOOKMARK [2][-]{subsection.2.8}{Conclusion}{section.2}% 10
\BOOKMARK [1][-]{section.3}{Testing Scientific Software: A Systematic Literature Review KANEWALA20141219}{}% 11
\BOOKMARK [2][-]{subsection.3.1}{Study Selection}{section.3}% 12
\BOOKMARK [2][-]{subsection.3.2}{Faults Categorization}{section.3}% 13
\BOOKMARK [3][-]{subsubsection.3.2.1}{Challenges due to Characteristics}{subsection.3.2}% 14
\BOOKMARK [3][-]{subsubsection.3.2.2}{Challenges due to Cultural Differences}{subsection.3.2}% 15
\BOOKMARK [2][-]{subsection.3.3}{Testing Methods}{section.3}% 16
\BOOKMARK [2][-]{subsection.3.4}{Limitations}{section.3}% 17
\BOOKMARK [2][-]{subsection.3.5}{Conclusion}{section.3}% 18
\BOOKMARK [1][-]{section.4}{Test Driven Development: A Survey Nanthaamornphong2017}{}% 19
\BOOKMARK [2][-]{subsection.4.1}{Methodology}{section.4}% 20
\BOOKMARK [2][-]{subsection.4.2}{Benefits of TDD}{section.4}% 21
\BOOKMARK [2][-]{subsection.4.3}{Testing Problems}{section.4}% 22
\BOOKMARK [2][-]{subsection.4.4}{Solutions to Testing Problems}{section.4}% 23
\BOOKMARK [2][-]{subsection.4.5}{Refactoring Problems}{section.4}% 24
\BOOKMARK [2][-]{subsection.4.6}{Solutions to Refactoring Problems}{section.4}% 25
\BOOKMARK [2][-]{subsection.4.7}{Challenges}{section.4}% 26
\BOOKMARK [2][-]{subsection.4.8}{Testing Practices}{section.4}% 27
\BOOKMARK [2][-]{subsection.4.9}{Limitations}{section.4}% 28
\BOOKMARK [2][-]{subsection.4.10}{conclusion}{section.4}% 29
\BOOKMARK [1][-]{section.5}{Claims About The Use Of Software Engineering Practices In Ssience Heaton2015207}{}% 30
\BOOKMARK [2][-]{subsection.5.1}{Methodology}{section.5}% 31
\BOOKMARK [2][-]{subsection.5.2}{Claims}{section.5}% 32
\BOOKMARK [3][-]{subsubsection.5.2.1}{Claims on Lifecycle}{subsection.5.2}% 33
\BOOKMARK [3][-]{subsubsection.5.2.2}{Claims on Requirements}{subsection.5.2}% 34
\BOOKMARK [3][-]{subsubsection.5.2.3}{Claims on Design}{subsection.5.2}% 35
\BOOKMARK [3][-]{subsubsection.5.2.4}{Claims on Testing}{subsection.5.2}% 36
\BOOKMARK [3][-]{subsubsection.5.2.5}{Claims on Validation and Verification}{subsection.5.2}% 37
\BOOKMARK [3][-]{subsubsection.5.2.6}{Claims on Refactoring}{subsection.5.2}% 38
\BOOKMARK [3][-]{subsubsection.5.2.7}{Claims on Documentation}{subsection.5.2}% 39
\BOOKMARK [3][-]{subsubsection.5.2.8}{Claims on Infrastructure}{subsection.5.2}% 40
\BOOKMARK [3][-]{subsubsection.5.2.9}{Claims on Reuse}{subsection.5.2}% 41
\BOOKMARK [3][-]{subsubsection.5.2.10}{Claims on Third-Party Software}{subsection.5.2}% 42
\BOOKMARK [3][-]{subsubsection.5.2.11}{Claims on Version Control}{subsection.5.2}% 43
\BOOKMARK [2][-]{subsection.5.3}{Conclusion}{section.5}% 44
\BOOKMARK [1][-]{section.6}{Best Practices For Scientific Computing 10.1371/journal.pbio.1001745}{}% 45
\BOOKMARK [2][-]{subsection.6.1}{Best Practices}{section.6}% 46
\BOOKMARK [3][-]{subsubsection.6.1.1}{Writing Code for People}{subsection.6.1}% 47
\BOOKMARK [3][-]{subsubsection.6.1.2}{Computer to do the Work}{subsection.6.1}% 48
\BOOKMARK [3][-]{subsubsection.6.1.3}{Incremental Changes}{subsection.6.1}% 49
\BOOKMARK [3][-]{subsubsection.6.1.4}{Repeating Yourself}{subsection.6.1}% 50
\BOOKMARK [3][-]{subsubsection.6.1.5}{Plan for Mistakes}{subsection.6.1}% 51
\BOOKMARK [3][-]{subsubsection.6.1.6}{Performance}{subsection.6.1}% 52
\BOOKMARK [3][-]{subsubsection.6.1.7}{Documentation}{subsection.6.1}% 53
\BOOKMARK [3][-]{subsubsection.6.1.8}{Collaborate}{subsection.6.1}% 54
\BOOKMARK [1][-]{section.7}{Synthesis}{}% 55
\BOOKMARK [2][-]{subsection.7.1}{Introduction}{section.7}% 56
\BOOKMARK [2][-]{subsection.7.2}{Requirements}{section.7}% 57
\BOOKMARK [3][-]{subsubsection.7.2.1}{Practices}{subsection.7.2}% 58
\BOOKMARK [3][-]{subsubsection.7.2.2}{Challenges}{subsection.7.2}% 59
\BOOKMARK [3][-]{subsubsection.7.2.3}{Possibilities}{subsection.7.2}% 60
\BOOKMARK [2][-]{subsection.7.3}{Design}{section.7}% 61
\BOOKMARK [3][-]{subsubsection.7.3.1}{Practices}{subsection.7.3}% 62
\BOOKMARK [3][-]{subsubsection.7.3.2}{Challenges}{subsection.7.3}% 63
\BOOKMARK [3][-]{subsubsection.7.3.3}{Possibilities}{subsection.7.3}% 64
\BOOKMARK [2][-]{subsection.7.4}{Development}{section.7}% 65
\BOOKMARK [3][-]{subsubsection.7.4.1}{Practices}{subsection.7.4}% 66
\BOOKMARK [3][-]{subsubsection.7.4.2}{Challenges}{subsection.7.4}% 67
\BOOKMARK [3][-]{subsubsection.7.4.3}{Possibilities}{subsection.7.4}% 68
\BOOKMARK [2][-]{subsection.7.5}{Testing}{section.7}% 69
\BOOKMARK [3][-]{subsubsection.7.5.1}{Practices}{subsection.7.5}% 70
\BOOKMARK [3][-]{subsubsection.7.5.2}{Challenges}{subsection.7.5}% 71
\BOOKMARK [3][-]{subsubsection.7.5.3}{Possibilities}{subsection.7.5}% 72
\BOOKMARK [2][-]{subsection.7.6}{Verification and Validation}{section.7}% 73
\BOOKMARK [3][-]{subsubsection.7.6.1}{Practices}{subsection.7.6}% 74
\BOOKMARK [3][-]{subsubsection.7.6.2}{Challenges}{subsection.7.6}% 75
\BOOKMARK [3][-]{subsubsection.7.6.3}{Possibilities}{subsection.7.6}% 76
\BOOKMARK [2][-]{subsection.7.7}{Maintenance and Usage}{section.7}% 77
\BOOKMARK [2][-]{subsection.7.8}{Best Practices}{section.7}% 78
\BOOKMARK [1][-]{section.8}{State Of The Art}{}% 79
\BOOKMARK [2][-]{subsection.8.1}{Understandability}{section.8}% 80
\BOOKMARK [2][-]{subsection.8.2}{Testability}{section.8}% 81
\BOOKMARK [2][-]{subsection.8.3}{Communication}{section.8}% 82
\BOOKMARK [2][-]{subsection.8.4}{Influence of the Core Papers}{section.8}% 83
\BOOKMARK [1][-]{section.9}{Future Direction}{}% 84
\BOOKMARK [1][-]{section.10}{Conclusion}{}% 85
\BOOKMARK [1][-]{section*.1}{References}{}% 86
