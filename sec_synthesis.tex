\subsection{Introduction}
In this section, I cross-examine the five core papers and categorize the key findings according to different phases (requirements, design, development, testing, and evaluation) of standard software development life-cycle. 
My hypothesis is scientific software has the ability to adopt software engineering practices used in general commercial and open source software development.

\subsection{Requirements}
Requirements gathering and analysis is the first step of any software development.
It is a collection of descriptions of the tasks that a software must do.
It can be as descriptive as a detailed mathematical functional specification to as short as high-level abstract statements.
This section discusses current practices of requirements gathering and analysis, risks associated with requirements, and possibilities in scientific software development. 

\subsubsection{Practices}
Scientist developers are unlikely to fully state the requirements so that the plan-based engineering methods are ineffective in the context of scientific software \cite{Storer2017}. 
Software engineers think of requirements specification as a complete document, whereas scientist developers think it as an outline into which additional features could be incorporated later \cite{Storer2017}\cite{Heaton2015207}. 
So, preparing requirement document is not an established practice to the scientific community.
Sometimes, they just informally collect requirements from end users.

Scientist developers focus more on the result than producing detailed requirement specifications.
This tendency leads them to lack of a good understanding of the need for proper requirements which in turn causes late delivery of the document and increases pressure in the time \cite{Heaton2015207}. 
Although some scientist developers want to create requirements for their project, they usually concentrate on the high-level requirements only.
Typically, these high-level requirements are sub-set of background knowledge of the scientists, and they think that converting these high-level requirements to lower-level requirements is something insignificant for the developers.
On the other hand, software developers may not have the required knowledge of science to decompose the requirements, thus project cost increases unnecessarily \cite{5069156}.
Even, scientists rely on the developers to prioritize the high-level requirements they produced, but the developers cannot properly prioritize the requirements and effectively develop the software due to the lack of scientific background \cite{Heaton2015207}. 

\subsubsection{Challenges}
There are several risks associated with the procedure because of the exploration of relatively unknown scientific phenomena. 
The scientific domain is inherently complex, and it constantly changes as new information gathered \cite{seforscience}.
This iterative nature of scientific domain makes scientist developer reluctant about producing proper requirements document. 
Also, the lack of understanding of the requirement is one of the major risks on making requirement specificaiton document prior development of scientific software \cite{Heaton2015207}.

\subsubsection{Possibilities}
Though there are different obstacles and risks associated with requirements but none of them are severe enough that a requirement document can not be produced.
Due to the dynamic and concurrent nature of the scientific software, agile practices for requirements gathering may fit better \cite{Storer2017}.
However, a scientific representation to prioritize the requirements for the developers may rectify this problem \cite{Heaton2015207}.

\subsection{Design}
The next step after requirements is design, which consists of architectural modules, data flow representation, and communications with external and third-party modules. This is one of the most underutilized practice in scientific software development. 
\subsubsection{Practices}
Typically, scientist developers concentrate more on functionality and don't think design can be a separate step in the software development process. 
In an interview, only two of twelve scientist developers indicate that they use design as a distinct software development step \cite{Heaton2015207}.
Some of them even consider redesign risks breaking the underlying science in the software, and nothing but a waste of time.
Their development work is mostly to expand the existing projects and that is why they don't see any importance in practicing redesign and somewhat reluctant to change the original design \cite{Segal2009}.
\subsubsection{Challenges}
Scientific software has data dependencies within the software. 
It is also challenging to identify the most appropriate parallelization strategy for scientific software algorithm \cite{seforscience}.
The presence of complex input/output patterns could also degrade performance. 
Additionally, in case of software or hardware failure, the need for fault tolerance and task migration mechanisms could also be challenging for the design phase. 
\subsubsection{Possibilities}
As a means of managing the complexity of scientific software, applying software pattern \cite{design_pattern_book} could be useful \cite{Heaton2015207}. 
Object-Oriented design also aids to develop good quality software, and any programming language utilizing Object-Oriented design illustration can necessarily help a project to be successful \cite{Storer2017} \cite{Heaton2015207}.
So, despite several challenges, it is still possible to alleviate different barriers to have a design phase in scientific software development. 

\subsection{Development}
The development phase of a scientific software differs from general software in many ways.
It reflect stages that are not present in many other types of software, including, model development, discretization, and numerical algorithm development.
The general class behavior of modeling for scientific simulations begins with equations.
In the case of translating the model from mathematical equation to computational representation, discretization and approximation are the two processes to go on simultaneously \cite{chapter1}. 
But, it depends on the level of understanding of specific sub-phenomena and available computer resources. 
Sometimes, scientists use their judgment to make other approximations.

Finding the appropriate numerical methods is the next stage in developing code for each of the models. 
We can use existing methods without any modifications in general software development but in the case of scientific software development, most of the time we need customization or development of a new method. 
After specifying the numerical algorithms, with the understanding of semantics, the translating process begins to convert into executable code. 
Still, there are differences because scientist developers work iteratively \cite{Segal2005} and requirement specifications develop gradually through the development cycle. 
\subsubsection{Practices}
Scientist developers tend to use agile methodology in their projects due to the dynamic and concurrent nature of this method \cite{Storer2017}.
It also helps them to cope up with the evolving list of requirements.
Nowadays, scientific software development is accepting new development methodologies, for instance, continuous integration, formal methods \cite{Storer2017}.
Continuous integration helps scientists to minimize the disruption caused by concurrent and continuous changes to software. 
On the other hand, formal method is useful for mathematical specification and verification of computer programs.

Test-driven development is an another comparatively new software engineering practice that scientist developers are increasingly employing to their projects \cite{Nanthaamornphong2017}.
It helps them to increase the quality of their software.
To be specific, it is useful in reliability and functionality. 
TDD also helps to decrease the number of problems in the early phase of the scientific software development. 
But, it may not be suitable for all scientific projects.
Additionally, if the test is not written good, TDD may not be useful at all.
So, writing good tests which is a difficult task, makes the TDD challenging.
\subsubsection{Challenges}
The development of scientific software is challenging because of the interdisciplinary nature of the work, the complexity of the underlying science, and other cultural and institutional challenges \cite{chapter1}\cite{KANEWALA20141219}\cite{Storer2017}\cite{Heaton2015207}.
Even the requirements are themselves can be the subject of the research that causes requirements to evolve during development cycle.
Highly specialized skill set required to balance continuous development with ongoing production requires open development with good contribution and distribution policies. 
Additionally, the monolithic nature of the software and the need for performance make it difficult to modularizing multi-component software to achieve separation of concerns. 
\subsubsection{Possibilities}
The development phase is quite challenging than any other phase in scientific software. 
But scientist developers already started using several existing methodologies. 
Software engineers should also keep in mind about the challenges scientists face while producing new methodologies. 
 
\subsection{Testing}
Traditional software development gets the benefit of using many available testing techniques, but in the case of scientific software development, use of these practices is somewhat limited \cite{Heaton2015207}. 
If some scientist developers use them, they execute poorly while the tests are not repeatable. 
However, scientists use tests to make sure that their knowledge of science is appropriate instead of checking the software is working correctly or not. 
In addition, testing is extensively complex in the scientific project because scientists are not familiar with the correct results.
Regarding this, experimental validation is somewhat impossible, that means scientists may not even get the desired answer \cite{5069156}.
Moreover, the developers tend to test code when development is about to finish, which restricts them to test the whole software instead of dividing it into objectively testable pieces.
\subsubsection{Practices}
Scientist developers use some of the available testing techniques \cite{KANEWALA20141219}\cite{Nanthaamornphong2017}.
Unit testing is widely used in the scientific community. 
Scientists like to use refactoring to extract testable units while unit testing. 
Many of them use integration testing after unit testing to make sure that every component works together as expected. 
Several projects like climate modeling projects use system testing to test the software as a whole. 
Only a few of the scientist developers use acceptance testing in order to check that programmers implemented any required functionality properly.
Regression testing is vey popular in the scientific community. 
Scientist like this testing because they compare the pervious output with the current output in every iteration of code changes.
\subsubsection{Challenges}
There are much more challenges of testing in scientific software than comparing to general software development \cite{KANEWALA20141219}.
The characteristics of the scientific software itself are challenging. 
It starts with test case development to producing expected test case output, test execution to test result interpretation. 
Another major challenge is due to the cultural differences between scientists and the software engineering community. 
Scientists usually have a limited understanding of the testing process where software engineers have limited understanding of the underlying science.
Sometimes, not applying the known testing method also makes testing challenging.
\subsubsection{Possibilities}
Despite many challenges, scientist developers are using some of the existing testing practices from general software development \cite{KANEWALA20141219}\cite{Nanthaamornphong2017}.
Test-driven development is also getting popular day by day. 
Maybe slight customization of the testing tools according to the necessity of the scientific community is needed to have widespread use of available testing techniques.


\subsection{Verification and Validation}
Verification is the process to make sure that the implementation of the model is correct. 
On the other hand, validation ensures that the mathematical model defines the physical phenomena correctly. 
In other words, developers verify the model by other forms of testing, while scientists validate a model by observations or experiments from the physical world \cite{Oberkampf2002209}. 
\subsubsection{Practices}
The scientific experts carefully calibrate model validation because the process of deriving a model involves many degrees of freedom \cite{Heaton2015207}. 
Similarly, applied mathematicians verify numerical methods to gain stability, accuracy, and order of convergence in addition to correctness. 
As many types of research going on, the implementation of numerical methods may need modifications from time to time. 
When such modifications happen, the entire verification and validation process needs to hand over the implementation to software engineers or developers who do not have domain or math knowledge and where no amount of specification is enough.
As the process has to be iterative with scientific judgment applied at every iteration, a close collaboration is necessary among various experts \cite{Monniaux:2008:PVF:1353445.1353446}. 
\subsubsection{Challenges}
Scientific software is challenging to validate because of the lack of comparable software or appropriate test oracle \cite{Heaton2015207}.
Some developers try to monitor the variables that change in a familiar manner to address this lack of external data and perform verification.
But scientists are not generally concerned with these variables, thus usefulness of this technique is limited.
In addition, scientist tends to look at the science more closely when an error occurs or any validation fails.
Sometimes, it is impractical to perform experimental validation due to the lack of information used to validate the software.
Moreover, the developers consider validation studies as a research or thesis because of the challenges in executing them, and they find it is difficult to validate each and every part of their software.
\subsubsection{Possibilities}
There are different understandings between scientists and software engineers about verification and validation \cite{Heaton2015207}.
Proper communication between them might be useful to reduce the gap of understanding.
Besides testing, the lack of test oracles affects verification and validation procedure also. 
But many of the scientists started pseudo oracle to solve the problem. 
So, there is always another way for scientific software if general software engineering practices cannot be used directly. 

\subsection{Maintenance and Usage}
The goal of scientific software development is usually responding to an immediate scientific need, so the developers release the software as soon as they have a minimal set of capabilities for some simulation. 
Accordingly, new findings in science and math almost continuously place new demands on the code, and that is why the development of computational modules almost never stops through the code lifecycle \cite{Storer2017}\cite{chapter1}.

Users of scientific software must have a basic understanding of the models with the approximations as well as the numerical methods they are using to obtain valid scientific results. 
Some people even argue in not using someone else's code in the scientific community who have not written at least a simplistic version of the code for their application \cite{5069155}.
Although that argument goes too far, the general belief in the scientific community is that users of scientific codes have a great responsibility to understand the capabilities and limitations of their tools.
\subsection{Best Practices}
There are several ways to reduce the complexity involved in developing scientific software \cite{10.1371/journal.pbio.1001745}. 
The focus of writing programs should be people, not computers. 
The program should be written in such a way that it makes the computer to do all the repeated tasks. 
It is better to work in small steps and make incremental changes.
Scientist developer should make plans for mistakes and only optimize the software after it works correctly. 
Working in collaboration and practices like code reviews can help reducing any unwanted complexity while developing the scientific software. 